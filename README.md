# memesearch
Imgur meme search
## Getting Started

### Prerequisites

* Node.js (https://nodejs.org/en/)

Clone or download the repository.

### Installing

Before running the app, you need to install all necessary packages. To do so, open the terminal in localization of downloaded project, and paste:

```
npm install
```

for NPM users
or
```
yarn install
```
for Yarn users.

When all packages will be installed, run:
```
npm start
```

for NPM users
or
```
yarn start
```
for Yarn users.

After a while, project will be available in your browser at:
```
http://localhost:3000/
```
