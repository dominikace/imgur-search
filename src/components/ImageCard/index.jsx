// @flow
import React from 'react';
import styled from 'styled-components';
import { Link } from 'react-router-dom';
import Video from '../Video';
import Image from '../Image';

const ImageWrapper = styled.div`
  margin: 1rem 0;
  border: 1px solid #ccc;
  padding: 2rem 1rem 1rem 1rem;
  border-radius: 4px;
  width: 100%;
  @media (min-width: 768px){
    width: 49%;
  }
  @media (min-width: 1200px){
    width: 32%;
  }
`;
const ImageCrop = styled.div`
    border-radius: 6px;
    width: 100%;
    max-height: 350px;
    overflow: hidden;
`;
const ImageTitle = styled.div`
  font-size: 1.65rem;
  color: #24292e;
  line-height: 1.875rem;
  padding: 1rem 0 2rem 0;
`;
type Props = {
  id: string,
  title: string,
  is_album: boolean,
  link?: string,
  images?: Array<Object>,
  mp4?: string,

}
const ImageCard = (props: Props) => {
  const detailsLink: string = props.is_album ? `/details/album/${props.id}` : `/details/${props.id}`;
  const imgUrl: string = props.images !== undefined && props.is_album
    ? props.images[0].link
    : props.link;

  return (
    <ImageWrapper>
      <ImageTitle>{props.title}</ImageTitle>
      <Link to={detailsLink}>
        <ImageCrop>
          {
            props.images !== undefined && props.images[0].mp4 !== undefined
            ? <Video mp4={props.images[0].mp4} />
            : <Image src={imgUrl} alt={props.title} />
          }
        </ImageCrop>
        <p>See details</p>
      </Link>
    </ImageWrapper>
  );
};
ImageCard.defaultProps = {
  link: '',
  images: [{ link: '' }],
  mp4: undefined,
};
export default ImageCard;
