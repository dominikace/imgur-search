// @flow
import React from 'react';
import styled from 'styled-components';
import Comment from '../Comment';

const CommentWrapper = styled.div`
  border: 1px solid #ccc;
  padding: 1rem;
  margin: 1rem 0;
`;
type Props = {
  comments: Array<Object>
}
const CommentsList = (props: Props) => (
  <div>
    <h3>Comments</h3>
    {
      props.comments !== undefined && props.comments.length > 0
      ? props.comments.map(comment =>
        (
          <CommentWrapper key={comment.id}>
            <Comment {...comment} />
          </CommentWrapper>))
      : 'No comments'
    }
  </div>
);

export default CommentsList;
