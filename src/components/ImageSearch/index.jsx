// @flow
import React from 'react';
import styled from 'styled-components';
import { Debounce } from 'react-throttle';

const Input = styled.input`
  padding: 0.5em 1.8em 0.5em 1em ;
  border: solid 1px #ccc;
  color: #444;
  border-radius: 60px;
  line-height: 1.15;
  outline: none;
  font-size: 1.38em;
  display: inline-block;
  min-width: 400px;
`;
const ClearSearch = styled.span`
  position: absolute;
  right: 12px;
  top: -10px;
  font-size: 1.5em;
  cursor: pointer;
`;

const InputWrapper = styled.span`
  position: relative;
`;
const Center = styled.div`
  text-align: center;
`;

type Props = {
  onSearch: (query: string) => void,
  clearSearch: () => void,
  query: string,
};

const ImageSearch = (props: Props) => (
  <Center>
    <InputWrapper>
      <Debounce time="1000" handler="onChange">
        <Input
          placeholder="Enter a phrase to search"
          onChange={e => (e.target.value.length !== 0 ? props.onSearch(e.target.value) : '')}
          defaultValue={props.query}
          autoFocus
        />
      </Debounce>
      {
          props.query !== undefined && props.query.length !== 0
          ?
            <ClearSearch onClick={props.clearSearch}>&#x2715;</ClearSearch>
          : ''
        }
    </InputWrapper>
  </Center>
);

export default ImageSearch;
