// @flow
import React from 'react';
import styled from 'styled-components';

const Vid = styled.video`
  width: 80% !important;
  height: auto !important;
`;
type Props = {
  mp4: string
}
const Video = (props: Props) => (
  <Vid width="320" height="240" controls>
    <source src={props.mp4} type="video/mp4" />
    <track kind="captions" {...props} />
    Your browser does not support the video tag.
  </Vid>
);

export default Video;
