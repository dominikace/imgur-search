// @flow
import React from 'react';
import styled from 'styled-components';
import DetailsImage from '../DetailsImage';

const DetailsWrapper = styled.div`
  margin: 1rem 0;
  border: 1px solid #ccc;
  padding: 2rem 1rem 1rem 1rem;
  border-radius: 4px;
  width: 100%;

`;
const ImagesWrapper = styled.div`
  text-align: center;
  padding: 2rem 0;
  img{
    width: 80%;
    height: auto;
  }
`;

const Title = styled.h1`
  font-size: 2rem;
  font-weight: 600;
`;
const Stats = styled.div`
font-size: 1rem;
  span {
    margin-right: 1rem;
  }
`;

type Props = {
  title: string,
  images: any, // todo: improve type
  author: any, // todo: improve type
  views: number,
  points: number
}
const DetailsBlock = (props: Props) => (
  <DetailsWrapper>
    <Title>{props.title}</Title>
    {props.author}

    <ImagesWrapper>
      {
        props.images.map(item => <DetailsImage {...item} key={item.id} />)
      }
    </ImagesWrapper>
    <Stats>
      <span><strong>Views:</strong> {props.views}</span>
      <span><strong>Points:</strong> {props.points}</span>
    </Stats>
  </DetailsWrapper>
);
export default DetailsBlock;
