// @flow
import React from 'react';
import styled from 'styled-components';

const Img = styled.img`
    border-radius: 6px;
    width: 100%;
    height: auto;
`;
type Props = {
  src: string,
  alt: string
}
const Image = (props: Props) => (
  <Img {...props} />
);

export default Image;
