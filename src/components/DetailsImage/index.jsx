// @flow
import React from 'react';
import styled from 'styled-components';

import Video from '../Video';
import Image from '../Image';

const ImageWrapper = styled.div`
  margin-bottom: 1rem;
  padding-bottom: 1rem;
  border-bottom: 1px solid #ccc;
`;

type Props = {
  title: string,
  link: string,
  mp4?: string,
}
const DetailsImage = (props: Props) => (
  <ImageWrapper>
    {
      props.mp4 !== undefined
      ? <Video mp4={props.mp4} />
      : <Image src={props.link} alt={props.title} />
    }
  </ImageWrapper>
);
DetailsImage.defaultProps = {
  mp4: undefined,
};
export default DetailsImage;
