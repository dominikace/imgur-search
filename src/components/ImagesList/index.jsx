// @flow
import React from 'react';
import styled from 'styled-components';
import ImageCard from '../ImageCard';
import ErrorBoundary from '../ErrorBoundary';

const ImagesWrapper = styled.div`
    text-align: center;
    padding: 0 1rem;
    text-align: center;
    display: flex;
    flex-direction: row;
    flex-wrap: wrap;
    justify-content: space-between;
    padding: 0 1rem;
`;
type Props = {
  images: Array<Object>
}
const ImagesList = (props: Props) => (
  <ImagesWrapper>
    {
    props.images !== undefined && props.images.length > 0
    ?
      <ErrorBoundary>
        {props.images.map(item => <ImageCard {...item} key={item.id} />)}
      </ErrorBoundary>
    : 'No memes to display'
  }
  </ImagesWrapper>
);
export default ImagesList;
