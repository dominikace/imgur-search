// @flow
import React from 'react';
import styled from 'styled-components';

const SubComments = styled.div`
  margin-left: 2rem;
  padding-top: 1rem;
`;
const Author = styled.div`
  font-weight: 600;
`;
type Props = {
  author: string,
  comment: string,
  children: Array<any>
}
const Comment = (props: Props) => {
  const subComments = props.children !== undefined
    ? props.children.map(subcomment =>
      <Comment key={subcomment.id} {...subcomment} />) : '';
  return (
    <div>
      <Author>{props.author}
      </Author>
      <div>{props.comment}</div>
      <SubComments> {subComments} </SubComments>
    </div>
  );
};

export default Comment;
