import React from 'react';
import { BounceLoader } from 'react-spinners';
import styled from 'styled-components';

const LoaderWrapper = styled.div`
  position: absolute;
  top: calc(50% - 30px);
  left: calc(50% - 30px);
`;
const Loading = () => (
  <LoaderWrapper>
    <BounceLoader
      color="#264272"
      size={60}
      loading
    />
  </LoaderWrapper>
);

export default Loading;
