// @flow
import React from 'react';
import styled from 'styled-components';
import ImageCard from '../ImageCard';
import ErrorBoundary from '../ErrorBoundary';

const ImagesWrapper = styled.div`
    text-align: center;
    padding: 0 1rem;
    text-align: center;
    display: flex;
    flex-direction: row;
    flex-wrap: wrap;
    justify-content: space-between;
    padding: 0 1rem;
`;
type Props = {
  images: Array<Object>
}
const ImagesList = (props: Props) => (
  <ImagesWrapper>
    <ErrorBoundary>
      {props.images.map(item => <ImageCard {...item} key={item.id} />)}
    </ErrorBoundary>
  </ImagesWrapper>
);
export default ImagesList;
