// @flow
import { combineReducers } from 'redux';

import { GET_IMAGES
  , GET_IMAGES_PAGE
  , GET_IMAGE_DETAILS
  , GET_ALBUM_DETAILS
  , GET_COMMENTS
  , GET_USER_IMAGES
  , GET_IMAGES_ERROR
  , CLEAR_STORE
  , GET_IMAGE_DETAILS_ERROR
  , GET_ALBUM_DETAILS_ERROR
  , GET_COMMENTS_ERROR
  , GET_USER_IMAGES_ERROR } from '../utils/actionsConstants';

import INIT_STORE from '../utils/initStore';

import type { StoreType, PolandballType, ErrorsType, Action, PolandballAction, ErrorAction }
  from '../utils/commonTypes';

const polandball = (state: PolandballType = INIT_STORE.polandball, action: PolandballAction) => {
  switch (action.type) {
    case GET_IMAGES:
      return {
        ...state,
        images: action.data !== undefined ? action.data.data : [],
      };
    case GET_IMAGES_PAGE:
      return {
        ...state,
        images: [
          ...(state.images !== undefined ? state.images : []),
          ...(action.data !== undefined ? action.data.data : []),
        ],
      };
    case GET_IMAGE_DETAILS:
      return {
        ...state,
        image: action.data !== undefined ? action.data.data : [],
      };
    case GET_ALBUM_DETAILS:
      return {
        ...state,
        album: action.data !== undefined ? action.data.data : [],
      };
    case GET_COMMENTS:
      return {
        ...state,
        comments: action.data !== undefined ? action.data.data : [],
      };
    case GET_USER_IMAGES:
      return {
        ...state,
        userImages: action.data !== undefined ? action.data.data : [],
      };
    default:
      return state;
  }
};

const errors = (state: ErrorsType = INIT_STORE.errors, action: ErrorAction) => {
  switch (action.type) {
    case GET_IMAGES_ERROR:
      return {
        ...state,
        imagesErrorMsg: `Images error: ${action.error}`,
      };
    case GET_IMAGE_DETAILS_ERROR:
      return {
        ...state,
        imageErrorMsg: `Image details error: ${action.error}`,
      };
    case GET_ALBUM_DETAILS_ERROR:
      return {
        ...state,
        albumErrorMsg: `Album details error: ${action.error}`,
      };
    case GET_COMMENTS_ERROR:
      return {
        ...state,
        commentsErrorMsg: `Comments details error: ${action.error}`,
      };
    case GET_USER_IMAGES_ERROR:
      return {
        ...state,
        userImagesErrorMsg: `Comments details error: ${action.error}`,
      };
    default:
      return state;
  }
};

const appReducer = combineReducers({ polandball, errors });

const rootReducer = (state: StoreType, action: Action) => {
  if (action.type === CLEAR_STORE) {
    return appReducer(INIT_STORE, action);
  }
  return appReducer(state, action);
};
export default rootReducer;
