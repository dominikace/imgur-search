// @flow
import { GET_IMAGES
  , GET_IMAGES_PAGE
  , GET_IMAGE_DETAILS
  , GET_ALBUM_DETAILS
  , GET_COMMENTS
  , GET_USER_IMAGES
  , CLEAR_STORE
  , GET_IMAGES_ERROR
  , GET_IMAGE_DETAILS_ERROR
  , GET_ALBUM_DETAILS_ERROR
  , GET_COMMENTS_ERROR
  , GET_USER_IMAGES_ERROR } from '../utils/actionsConstants';
import type { Action } from '../utils/commonTypes';

type Dispatch = (action: Action | Promise<Action>) => Promise<any>;

const IMGUR_API_CLIENT = 'ba6ed20d1e1691e';
const getSettings = {
  method: 'GET',
  headers: {
    Authorization: `Client-ID ${IMGUR_API_CLIENT}`,
  },
};
// fetch template
function fetchTemplate(url: string, requestSettings = getSettings) {
  return fetch(url, requestSettings).then((response) => {
    if (response.status >= 200 && response.status < 300) {
      return response.json();
    }
    const error: any = new Error(response.statusText || response.status);
    error.response = response;
    return Promise.reject(error);
  });
}

// Fetch all polandball images
function fetchImagesJSON(searchTerm: string = 'polandball', page: number = 0) {
  const url: string = `https://api.imgur.com/3/gallery/search/${page}?q=${searchTerm}`;
  return fetchTemplate(url, getSettings);
}
export function fetchImages(searchTerm: string, page: number) {
  return (dispatch: Dispatch) => fetchImagesJSON(searchTerm, page)
    .then((json: Object) => {
      if (page === 0) { return dispatch({ type: GET_IMAGES, data: json }); }
      return dispatch({ type: GET_IMAGES_PAGE, data: json });
    })
    .catch((error: any) => {
      dispatch({ type: GET_IMAGES_ERROR, error });
    });
}

// fetch  image
function fetchImageJSON(imageHash: string) {
  const url: string = `https://api.imgur.com/3/gallery/image/${imageHash}`;
  return fetchTemplate(url, getSettings);
}
export function fetchImage(imageHash: string) {
  return (dispatch: Dispatch) => fetchImageJSON(imageHash)
    .then((json: Object) => dispatch({ type: GET_IMAGE_DETAILS, data: json }))
    .catch((error: any) => {
      dispatch({ type: GET_IMAGE_DETAILS_ERROR, error });
    });
}

// fetch comments
function fetchCommentsJSON(itemHash: string) {
  const url: string = `https://api.imgur.com/3/gallery/${itemHash}/comments/`;
  return fetchTemplate(url, getSettings);
}
function fetchComments(albumHash: string) {
  return (dispatch: Dispatch) => fetchCommentsJSON(albumHash)
    .then((json: Object) => dispatch({ type: GET_COMMENTS, data: json }))
    .catch((error: any) => {
      dispatch({ type: GET_COMMENTS_ERROR, error });
    });
}
// fetch album
function fetchAlbumJSON(albumHash: string) {
  const url: string = `https://api.imgur.com/3/gallery/album/${albumHash}`;
  return fetchTemplate(url, getSettings);
}
function fetchAlbum(albumHash: string) {
  return (dispatch: Dispatch) => fetchAlbumJSON(albumHash)
    .then((json: Object) => dispatch({ type: GET_ALBUM_DETAILS, data: json }))
    .catch((error: any) => {
      dispatch({ type: GET_ALBUM_DETAILS_ERROR, error });
    });
}
// fetch album details
export function fetchAlbumDetails(albumHash: string) {
  return (dispatch: Dispatch) => Promise.all([
    dispatch(fetchAlbum(albumHash)),
    dispatch(fetchComments(albumHash)),
  ]).then(() => {
    // console.log('Req completed');
  });
}

// fetch image details
export function fetchImageDetails(imageHash: string) {
  return (dispatch: Dispatch) => Promise.all([
    dispatch(fetchImage(imageHash)),
    dispatch(fetchComments(imageHash)),
  ]).then(() => {
    // console.log('Req completed');
  });
}

// fetch user memes
function fetchUserJSON(userName: string) {
  const url: string = `https://api.imgur.com/3/account/${userName}/submissions/`;
  return fetchTemplate(url, getSettings);
}
export function fetchUser(userName: string) {
  return (dispatch: Dispatch) => fetchUserJSON(userName)
    .then((json: Object) => dispatch({ type: GET_USER_IMAGES, data: json }))
    .catch((error: any) => {
      dispatch({ type: GET_USER_IMAGES_ERROR, error });
    });
}
export function clearStore() {
  return (dispatch: Dispatch) =>
    dispatch({ type: CLEAR_STORE });
}
