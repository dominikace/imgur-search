import { injectGlobal } from 'styled-components';

injectGlobal`
*,
*:before,
*:after {
	box-sizing: border-box;
	margin: 0;
	padding: 0;
}
img, svg, a{
		transition: all 0.1s ease;
}
html, body, #root {
  width: 100%;
  padding: 0;
  margin: 0;
	color: #444;
}
html{
	box-sizing: border-box;
}
textarea, input, textarea:focus, input:focus, textarea:active, input:active{
	outline: none;
	background: none;
}
a, a:hover{
	text-decoration: none;
}
a{
	color: #264272;
}
a:hover {
	color: #162e56;
}
`;
