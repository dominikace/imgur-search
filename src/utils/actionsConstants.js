// @flow
export const GET_IMAGES: string = 'GET_IMAGES';
export const GET_IMAGES_PAGE: string = 'GET_IMAGES_PAGE';
export const GET_IMAGE_DETAILS: string = 'GET_IMAGE_DETAILS';
export const GET_ALBUM_DETAILS: string = 'GET_ALBUM_DETAILS';
export const GET_COMMENTS: string = 'GET_COMMENTS';
export const GET_USER_IMAGES: string = 'GET_USER_IMAGES';
export const CLEAR_STORE: string = 'CLEAR_STORE';

export const GET_IMAGES_ERROR: string = 'GET_IMAGES_ERROR';
export const GET_IMAGE_DETAILS_ERROR: string = 'GET_IMAGE_DETAILS_ERROR';
export const GET_ALBUM_DETAILS_ERROR: string = 'GET_ALBUM_DETAILS_ERROR';
export const GET_COMMENTS_ERROR: string = 'GET_COMMENTS_ERROR';
export const GET_USER_IMAGES_ERROR: string = 'GET_USER_IMAGES_ERROR';
