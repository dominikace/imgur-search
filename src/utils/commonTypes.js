// @flow
import { GET_IMAGES
  , GET_IMAGES_PAGE
  , GET_IMAGE_DETAILS
  , GET_ALBUM_DETAILS
  , GET_COMMENTS
  , GET_USER_IMAGES
  , GET_IMAGES_ERROR
  , GET_IMAGE_DETAILS_ERROR
  , GET_ALBUM_DETAILS_ERROR
  , GET_COMMENTS_ERROR
  , GET_USER_IMAGES_ERROR } from '../utils/actionsConstants';

export type PolandballType = {
  userImages: Array<Object>,
  images: Array<Object>,
  image: Object,
  album: Object,
  comments: Array<Object>,
}
export type ErrorsType = {
  imagesErrorMsg: string,
  imageErrorMsg: string,
  albumErrorMsg: string,
  commentsErrorMsg: string,
  userImagesErrorMsg: string
}
export type StoreType = {
  polandball: PolandballType,
  errors: ErrorsType
}
export type PolandballAction = { type: typeof GET_IMAGES, data?: { data: Array<Object> }}
  | { type: typeof GET_IMAGES_PAGE, data?: { data: Array<Object> }}
  | { type: typeof GET_IMAGE_DETAILS, data?: Object }
  | { type: typeof GET_ALBUM_DETAILS, data?: Object }
  | { type: typeof GET_COMMENTS, data?: Object }
  | { type: typeof GET_USER_IMAGES, data?: Object };

export type ErrorAction = { type: typeof GET_IMAGES_ERROR, error: any }
  | { type: typeof GET_IMAGE_DETAILS_ERROR, error: any }
  | { type: typeof GET_ALBUM_DETAILS_ERROR, error: any }
  | { type: typeof GET_COMMENTS_ERROR, error: any }
  | { type: typeof GET_USER_IMAGES_ERROR, error: any };

export type Action = PolandballAction | ErrorAction;
