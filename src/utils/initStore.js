// @flow
import type { StoreType } from './commonTypes';

const INIT_STORE:StoreType = {
  polandball: {
    images: [],
    image: {},
    album: {},
    userImages: [],
    comments: [],
  },
  errors: {
    imagesErrorMsg: '',
    imageErrorMsg: '',
    albumErrorMsg: '',
    commentsErrorMsg: '',
    userImagesErrorMsg: '',
  },
};
export default INIT_STORE;
