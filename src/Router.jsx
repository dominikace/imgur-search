// @flow
import React from 'react';
import { BrowserRouter, Route, Switch, Link } from 'react-router-dom';
import styled from 'styled-components';

import NotFound from './pages/NotFound';
import ImagesGallery from './pages/ImagesGallery';
import ImageDetails from './pages/ImageDetails';
import AlbumDetails from './pages/AlbumDetails';
import UserDetails from './pages/UserDetails';

const Title = styled.h1`
    font-size: 2rem;
    text-align: center;
    margin: 2rem 0;
`;

const Router = () => (
  <BrowserRouter>
    <div>
      <Title><Link to="/">Imgur memes</Link></Title>
      <Switch>
        <Route path="/" exact component={ImagesGallery} />
        <Route path="/user/:id" exact component={UserDetails} />
        <Route path="/details/:id" exact component={ImageDetails} />
        <Route path="/details/album/:id" exact component={AlbumDetails} />
        <Route component={NotFound} />
      </Switch>
    </div>
  </BrowserRouter>
);
export default Router;
