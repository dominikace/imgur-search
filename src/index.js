// @flow
import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { createStore, applyMiddleware } from 'redux';
import thunk from 'redux-thunk';
import { composeWithDevTools } from 'redux-devtools-extension';

import './utils/globalStyles';
import Router from './Router';

import rootReducer from './reducers';

import type { StoreType } from './utils/commonTypes';
import INIT_STORE from './utils/initStore';

const store: StoreType = createStore(
  rootReducer,
  INIT_STORE, composeWithDevTools(applyMiddleware(thunk)),
);
// store.subscribe(() => {
// });

const App = () => (
  <Provider store={store}>
    <Router />
  </Provider>
);

ReactDOM.render(
  <App />
  , document.getElementById('root'),
);
