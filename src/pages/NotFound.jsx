import React from 'react';
import Container from './structure/Container';

const NotFound = () => (
  <Container>
    Page not found
  </Container>
);

export default NotFound;
