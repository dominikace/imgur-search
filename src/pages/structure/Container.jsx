// @flow
import * as React from 'react';
import styled from 'styled-components';

const AppContainer = styled.div`
  width: 100%;
  padding-right: 15px;
  padding-left: 15px;
  margin: 4rem auto;

  @media (min-width:576px) {
    max-width: 540px
  }
  @media (min-width:768px) {
    max-width: 720px
  }
  @media (min-width:992px) {
    max-width: 960px
  }
  @media (min-width:1200px) {
    max-width: 1140px
  }
`;
type Props = {
  children?: React.Node
}
const Container = (props: Props) => (
  <AppContainer>
    {props.children}
  </AppContainer>
);
Container.defaultProps = {
  children: {},
};
export default Container;
