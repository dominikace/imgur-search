// @flow
import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';

import type { StoreType }
  from '../utils/commonTypes';
import { fetchAlbumDetails } from '../actions';

import Container from './structure/Container';
import DetailsBlock from '../components/DetailsBlock';
import CommentsList from '../components/CommentsList';
import Loading from '../components/Loading';

type Props = {
  fetchAlbumDetails: (id: string) => void,
  album: Object,
  comments: Array<Object>,
  match: any
}
type State = {
  loading: boolean
}
class AlbumDetails extends Component<Props, State> {
  constructor(props: Props) {
    super(props);
    this.state = {
      loading: true,
    };
  }
  componentDidMount() {
    this.props.fetchAlbumDetails(this.props.match.params.id);
  }
  componentWillReceiveProps(newProps) {
    if (newProps.album !== undefined && this.state.loading) {
      this.setState({ loading: false });
    }
  }
  render() {
    const images = this.props.album !== undefined
      ? this.props.album.images
      : [];
    const title = this.props.album !== undefined
      ? this.props.album.title : '';

    const userLink = this.props.album !== undefined
      ? `/user/${this.props.album.account_url}`
      : '';
    const author = <div> by <Link to={userLink}>{this.props.album !== undefined ? this.props.album.account_url : ''}</Link></div>;
    return (
      <Container>
        {
          this.state.loading
            ? <Loading />
            :
            <div>
              <DetailsBlock
                title={title}
                images={images}
                author={author}
                views={this.props.album.views}
                points={this.props.album.points}
              />
              <CommentsList comments={this.props.comments} />
            </div>
            }
      </Container>
    );
  }
}

function mapStateToProps(state: StoreType) {
  return {
    album: state.polandball.album,
    comments: state.polandball.comments,
  };
}
export default connect(mapStateToProps, { fetchAlbumDetails })(AlbumDetails);
