// @flow
import React, { Component } from 'react';
import { connect } from 'react-redux';

import type { StoreType }
  from '../utils/commonTypes';
import { fetchImages, clearStore } from '../actions';

import Container from './structure/Container';
import ImageSearch from '../components/ImageSearch';
import ImagesList from '../components/ImagesList';
import Loading from '../components/Loading';

type Props = {
  fetchImages: (query?: string, page?: number) => void,
  clearStore: () => void,
  images: Array<Object>
}
type State = {
  searchQuery: string,
  page: number,
  lastLength: number,
  loading: boolean,
  loadingNextPage: boolean,
}
class ImagesGallery extends Component<Props, State> {
  constructor(props: Props) {
    super(props);
    this.state = {
      searchQuery: '',
      page: 0,
      loading: true,
      loadingNextPage: false,
      lastLength: 0,
    };
  }
  componentDidMount() {
    this.props.fetchImages();
    window.addEventListener('scroll', this.onScroll, false);
  }
  componentWillReceiveProps(newProps) {
    if (newProps.images !== undefined && (this.state.loading || this.state.loadingNextPage)) {
      this.setState({ loading: false, loadingNextPage: false });
    }

    if (newProps.images !== undefined && newProps.images.length === this.props.images.length) {
      this.setState({ loadingNextPage: false });
    }
  }
  componentWillUnmount() {
    window.removeEventListener('scroll', this.onScroll, false);
  }

  onScroll = () => {
    if (
      (window.innerHeight + window.scrollY) >= (document.body.offsetHeight - 500) &&
       this.props.images.length !== this.state.lastLength && !this.state.loadingNextPage
    ) {
      this.onPageChange();
    }
  }
  onSearch = (query: string) => {
    if (query !== this.state.searchQuery) {
      this.props.fetchImages(query, 0);
      this.setState({
        searchQuery: query,
        page: 0,
        lastLength: 0,
        loading: true,
        loadingNextPage: false,
      });
    }
  }

  onPageChange = () => {
    const newPage = this.state.page + 1;
    const searchQuery = this.state.searchQuery !== '' ? this.state.searchQuery : undefined;
    this.props.fetchImages(searchQuery, newPage);
    this.setState({ page: newPage, loadingNextPage: true, lastLength: this.props.images.length });
  }

  clearSearch = () => {
    this.props.clearStore();
    this.setState({ searchQuery: '', page: 0 });
  }
  render() {
    return (
      <Container>
        <ImageSearch
          key={this.state.searchQuery}
          query={this.state.searchQuery}
          onSearch={this.onSearch}
          clearSearch={this.clearSearch}
        />
        {
          this.state.loading
            ? <Loading />
            : <ImagesList images={this.props.images} />
        }
        {
          this.state.loadingNextPage
            ? <div>Loading...</div>
            : ''
        }
      </Container>);
  }
}
function mapStateToProps(state: StoreType) {
  return { images: state.polandball.images };
}
export default connect(mapStateToProps, { fetchImages, clearStore })(ImagesGallery);
