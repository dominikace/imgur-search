// @flow
import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';

import type { StoreType }
  from '../utils/commonTypes';
import { fetchImageDetails } from '../actions';

import Container from './structure/Container';
import DetailsBlock from '../components/DetailsBlock';
import CommentsList from '../components/CommentsList';
import Loading from '../components/Loading';

type Props = {
  fetchImageDetails: (id: string) => void,
  image: Object,
  comments: Array<Object>,
  match: any
}
type State = {
  loading: boolean
}
class ImageDetails extends Component<Props, State> {
  constructor(props: Props) {
    super(props);
    this.state = {
      loading: true,
    };
  }
  componentDidMount() {
    this.props.fetchImageDetails(this.props.match.params.id);
  }
  componentWillReceiveProps(newProps) {
    if (newProps.image !== undefined && this.state.loading) {
      this.setState({ loading: false });
    }
  }
  render() {
    const image = this.props.image !== undefined
      ? [this.props.image] : [];

    const title = this.props.image !== undefined
      ? this.props.image.title : '';

    const userLink = this.props.image !== undefined
      ? `/user/${this.props.image.account_id}`
      : '';
    const author = <div> by <Link to={userLink}>{this.props.image !== undefined ? this.props.image.account_url : ''}</Link></div>;
    return (
      <Container>  {
          this.state.loading
            ? <Loading />
            :
            <div>
              <DetailsBlock
                title={title}
                images={image}
                author={author}
                views={this.props.image.views}
                points={this.props.image.points}
              />
              <CommentsList comments={this.props.comments} />
            </div>
        }
      </Container>
    );
  }
}

function mapStateToProps(state: StoreType) {
  return {
    image: state.polandball.image,
    comments: state.polandball.comments,
  };
}
export default connect(mapStateToProps, { fetchImageDetails })(ImageDetails);
