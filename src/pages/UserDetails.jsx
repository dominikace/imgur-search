// @flow
import React, { Component } from 'react';
import { connect } from 'react-redux';

import type { StoreType }
  from '../utils/commonTypes';
import { fetchUser } from '../actions';

import Container from './structure/Container';
import ImagesList from '../components/ImagesList';
import Loading from '../components/Loading';

type Props = {
  fetchUser: (userName: string) => void,
  userImages: Array<Object>,
  match: any
}
type State = {
  loading: boolean,
}
class UserDetails extends Component<Props, State> {
  constructor(props: Props) {
    super(props);
    this.state = {
      loading: true,
    };
  }
  componentDidMount() {
    this.props.fetchUser(this.props.match.params.id);
  }
  componentWillReceiveProps(newProps) {
    if (newProps.userImages !== undefined && this.state.loading) {
      this.setState({ loading: false });
    }
  }

  render() {
    return (
      <Container>
        <h2>Memes by {this.props.match.params.id}</h2>
        {
          this.state.loading
            ? <Loading />
            : <ImagesList images={this.props.userImages} />
        }
      </Container>
    );
  }
}

function mapStateToProps(state: StoreType) {
  return { userImages: state.polandball.userImages };
}
export default connect(mapStateToProps, { fetchUser })(UserDetails);
